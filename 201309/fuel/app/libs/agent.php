<?php
class Agent extends Fuel\Core\Agent
{
    public static function _init()
    {
        parent::_init();
		
		// スマートデバイス
        $sd_list = array(
						'iPhone',
						'iPod',
						'iPad',
						'Android',
						'IEMobile',
						'dream',
						'CUPCAKE',
						'blackberry9500',
						'blackberry9530',
						'blackberry9520',
						'blackberry9550',
						'blackberry9800',
						'webOS',
						'incognito',
						'webmate'
		);

        $pattern = '/'.implode('|', $sd_list).'/i';
        static::$properties['x_issmartdevice'] = preg_match($pattern, static::$user_agent) ? true : false;
		
		// ケータイ
        $mp_list = array(
						'DoCoMo',
						'KDDI',
						'DDIPOKET',
						'UP.Browser',
						'J-PHONE',
						'Vodafone',
						'SoftBank',
		);

        $pattern = '/'.implode('|', $mp_list).'/i';
        static::$properties['x_isktai'] = preg_match($pattern, static::$user_agent) ? true : false;
		
    }
	
	/*
	 * スマートデバイス判別
	 *
	 * @access  public
	 * @return  bool
	 */
    public static function is_smartdevice()
    {
        return static::$properties['x_issmartdevice'];
    }
	
	/*
	 * ケータイ判別
	 *
	 * @access  public
	 * @return  bool
	 */
    public static function is_ktai()
    {
        return static::$properties['x_isktai'];
    }
	
	/*
	 * デバイス判別
	 * 
	 * @access  public
	 * @return  "pc" or "sp" or "tb" or "mp"
	 */
	public static function get_carrier()
	{
		if ((strpos($_SERVER["HTTP_USER_AGENT"], 'Android') !== false) && (strpos($_SERVER["HTTP_USER_AGENT"], 'Mobile') !== false) || (strpos($_SERVER["HTTP_USER_AGENT"], 'iPhone') !== false) || (strpos($_SERVER["HTTP_USER_AGENT"], 'Windows Phone') !== false)) {
			// スマートフォンからアクセスされた場合
			return "sp";

		} elseif ((strpos($_SERVER["HTTP_USER_AGENT"], 'Android') !== false) || (strpos($_SERVER["HTTP_USER_AGENT"], 'iPad') !== false)) {
			// タブレットからアクセスされた場合
			return "tb";

		} elseif ((strpos($_SERVER["HTTP_USER_AGENT"], 'DoCoMo') !== false) || (strpos($_SERVER["HTTP_USER_AGENT"], 'KDDI') !== false) || (strpos($_SERVER["HTTP_USER_AGENT"], 'SoftBank') !== false) || (strpos($_SERVER["HTTP_USER_AGENT"], 'Vodafone') !== false) || (strpos($_SERVER["HTTP_USER_AGENT"], 'J-PHONE') !== false)) {
			// 携帯からアクセスされた場合
			return "mp";

		} else {
			// その他（PC）からアクセスされた場合
			return "pc";
		}		
	}
}

?>
