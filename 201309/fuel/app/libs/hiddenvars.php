<?php
/*
 * CakePHPのhiddenVarsみたいなやつ。　※二次元配列まで対応。$arrayは空文字不可。
 */

class Hiddenvars
{
    public static function post($array)
    {
		Hiddenvars::exec($array, "post");
	}
	
    public static function get($array)
    {
		Hiddenvars::exec($array, "get");
	}
	
    public static function exec($array, $method)
    {
		if(is_array(Input::$method($array)))
		{
			foreach(Input::$method($array) as $param_group => $group_array)
			{
				if(is_array($group_array))
				{
					foreach($group_array as $key => $value)
					{
						?> <input type="hidden" name="<?=$array?>[<?=$param_group?>][<?=$key?>]" value="<?=$value?>" /> <?php
					}
				}else{
					?> <input type="hidden" name="<?=$array?>[<?=$param_group?>]" value="<?=$group_array?>" /> <?php
				}
			}
		}else{
			?> <input type="hidden" name="<?=$array?>" value="<?=Input::$method($array)?>" /> <?php
		}
    }
}

?>
