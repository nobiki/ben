<?php

namespace Fuel\Migrations;

class Create_ramens
{
	public function up()
	{
		\DBUtil::create_table('ramens', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'shop_name' => array('constraint' => 255, 'type' => 'varchar'),
			'area_name' => array('constraint' => 255, 'type' => 'varchar'),
			'ramen_type' => array('constraint' => 255, 'type' => 'varchar'),
			'created_at' => array('type' => 'datetime'),
			'updated_at' => array('type' => 'datetime'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('ramens');
	}
}