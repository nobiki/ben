<!DOCTYPE html>
<html>
<head>
<?php echo $partials['head']; ?>
<title></title>
</head>

<body>
------- MB ------- <br />
<div id="header">
<?php echo $partials['header']; ?>
</div>

<div id="content">
<?php echo $content; ?>
</div>

<div id="footer">
<?php echo $partials['footer']; ?>
</div>

</body>
</html>