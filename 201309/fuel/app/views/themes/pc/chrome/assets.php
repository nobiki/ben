<?php echo $head; ?>
<!-- begin chrome/assets -->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script src="<?=$jquery; ?>"></script>
<script src="<?=$underscore; ?>"></script>
<script src="<?=$backbone; ?>"></script>

<link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
<script src="/assets/js/bootstrap.min.js"></script>

<script src="/assets/js/models/app.js"></script>
<script src="/assets/js/views/app.js"></script>
<script src="/assets/js/app.js"></script>

<?php
	switch(Uri::segment(1))
	{
		case "top":
		case "sample":
			?>
			<script src="/assets/js/models/rider.js?<?=time()?>"></script>
			<script src="/assets/js/views/<?=Uri::segment(1)?><?=Uri::segment(2)?>.js?<?=time()?>"></script>
			<script src="/assets/js/<?=Uri::segment(1)?><?=Uri::segment(2)?>.js?<?=time()?>"></script>
			<?	break;
		default:
			?>
			<script src="/assets/js/models/top.js?<?=time()?>"></script>
			<script src="/assets/js/views/top.js?<?=time()?>"></script>
			<script src="/assets/js/top.js?<?=time()?>"></script>
			<?	break;
	}
?>
<!-- end chrome/assets -->
