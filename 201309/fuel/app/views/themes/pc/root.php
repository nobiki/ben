<!DOCTYPE html>
<html>
<head>
<?php echo $partials['head']; ?>
<title>JavascriptでMVC?アプリケーション!</title>
</head>

<body style="padding-top: 70px;text-align: left">
<?php echo $partials['header']; ?>

<div class="container">
<?php echo $content; ?>
</div>

<?php echo $partials['footer']; ?>

</body>
</html>