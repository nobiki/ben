<div class="navbar navbar-inverse navbar-fixed-top">
<div class="container">
  <div class="navbar-header">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	  <span class="icon-bar"></span>
	  <span class="icon-bar"></span>
	  <span class="icon-bar"></span>
	</button>
	<a class="navbar-brand" href="/">JavascriptでMVC?アプリケーション!</a>
	<ul class="nav navbar-nav">
      <li><a href="/sample/1/">サンプル1</a></li>
      <li><a href="/sample/2/">サンプル2</a></li>
      <li><a href="https://bitbucket.org/obiki/ben1309" target="_blank">ソース</a></li>
	</ul>
  </div>
</div>
</div>