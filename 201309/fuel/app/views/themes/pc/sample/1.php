<!-- 昭和のライダー一覧 -->
<table id="Dom_1" class="table table-bordered">
  <tbody>
  <tr><td><input type="button" class="btn btn-primary Get" value="更新" /></td><td colspan="2">昭和のライダー一覧</td></tr>
  <tr>
    <th>id</th>
    <th>ライダー名</th>
    <th>放映時期</th>
  </tr>
  </tbody>
</table>

<!-- 平成のライダー一覧 -->
<table id="Dom_2" class="table table-bordered">
  <tr><td><input type="button" class="btn btn-primary Get" value="更新" /></td><td colspan="2">平成のライダー一覧</td></tr>
  <tbody>
  <tr>
    <th>id</th>
    <th>ライダー名</th>
    <th>放映時期</th>
  </tr>
  </tbody>
</table>




<!-- テンプレート -->
<script type="text/template" id="rider_row_template">
  <tr data-row="rider">
    <td><%= id %></td>
    <td><%= rider_name %></td>
    <td><%= oa_type %></td>
  </tr>
</script>














<style>
	#Dom_1 {
		float:left;
		width: 40%;
		margin-top: 2px;
		margin-right: 20px;
	}
	#Dom_2 {
		width: 40%;
		margin-top: 2px;
	}
</style>