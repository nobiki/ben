<?php
/**
 * The development database settings. These get merged with the global settings.
 */

return array(
	'default' => array(
		'connection'  => array(
			'dsn'        => 'mysql:host=localhost;dbname=ben1309_dev',
			'username'   => 'root',
			'password'   => '4444',
		),
	),
);
