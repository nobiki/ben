<?php
return array(
	'_root_'  => 'top/index',  // The default route
	'_404_'   => '404',    // The main 404 route
	
	'backend' => 'backend/top/index',
	'backend/(:segment)' => 'backend/$1/index',
);