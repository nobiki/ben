<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.6
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_App extends Controller
{
	public $assign = array();			// Controller側：　$this->assign([key], [value])
	public $assign_safe = array();		// Controller側：　$this->assign([key], [value], "safe")
	public $assign_layout = array();	// Controller側：　$this->assign([key], [value], "layout")
	public $layout = "root";				// Controller側：　$this->layout = "blank";
	
	private $renderFlag = false;
	private $prefix = "";
	
	/*
	 * before
	 *
	 * @access  public
	 * @return  
	 */
	function before()
	{
		parent::before();

		//--- テーマの設定 [ここから]
		$this->theme = Theme::instance();
		
		// 基本デバイス振り分け
		switch(Agent::get_carrier())
		{
			case "mp":
				$this->theme->active('mp');
				break;
			case "sp":
				$this->theme->active('sp');
				break;
			case "tb":
				$this->theme->active('tb');
				break;
			case "pc":
			default:
				$this->theme->active('pc');
				break;
		}
		
		// レイアウト振り分け
		switch(Uri::segment(1))
		{
			case "backend":
				$this->prefix = Uri::segment(1).DS;

				switch(Agent::get_carrier())
				{
					case "mp":
					case "sp":
					case "tb":
					case "pc":
					default:
						$this->theme->active('pc');
						break;
				}
				break;
			default:
				$prefix = "";
				break;
		}

		// vendor scripts load
		$vendor_scripts = array(
			'backbone' => "data:text/javascript;base64,".base64_encode(file_get_contents(VENDORPATH."components/backbone/backbone-min.js")),
			'underscore' => "data:text/javascript;base64,".base64_encode(file_get_contents(VENDORPATH."components/underscore/underscore-min.js")),
			'jquery' => "data:text/javascript;base64,".base64_encode(file_get_contents(VENDORPATH."components/jquery/jquery.min.js")),
		);
		
		$this->theme->set_chrome('head', 'chrome/'.$this->prefix.'assets', 'head')->set($vendor_scripts);
		$this->theme->set_partial('head', $this->prefix.'head')->set("title", "Fuel 1.6 skel");
		$this->theme->set_partial('header', $this->prefix.'header');
		$this->theme->set_partial('footer', $this->prefix.'footer');
		//--- テーマの設定 [ここまで]
	}

	/*
	 * after
	 *
	 * @access  public
	 * @return  
	 */
	public function after($response)
	{
		if(!$this->renderFlag)
		{
			// レイアウトが明示されていればそれを使う
			$this->theme->set_template($this->prefix.$this->layout);
		
			// コントローラが明示されていなければ自身のコントローラ名を使う
			$controller = str_replace("_", DIRECTORY_SEPARATOR, str_replace("controller_", "", mb_strtolower($this->request->route->controller)));

			// アクションが明示されていればそれを使う
			$action = $this->request->route->action;
			
			$this->render($controller, $action);
		}
		
		if (empty($response) or ! $response instanceof Response)
		{
			$response = Response::forge($this->theme->render());
		}
		return parent::after($response);
	}
	
	/*
	 * 独自render関数（controller側で普通にset_partialしても良し）
	 *
	 * @access  public
	 * @return  
	 */
	public function render($controller = "", $action = "index", $assign = array())
	{
		// 追加assign情報
		foreach($assign as $k => $v)
		{
			$this->assign[$k] = $v;
		}
		
		$this->renderFlag = true;
		$this->theme->set_template($this->prefix.$this->layout)->set($this->assign_layout);
		$this->theme->get_template()->set('content',$this->theme->view($controller.DS.$action)->set($this->assign)->set_safe($this->assign_safe));
	}
	
	/*
	 * 独自assign関数
	 *
	 * @access  public
	 * @return  
	 */
	public function assign($key, $value, $mode = false)
	{
		if("safe" == $mode)
		{
			$this->assign_safe[$key] = $value;
		}else if("layout" == $mode){
			$this->assign_layout[$key] = $value;
		}else{
			$this->assign[$key] = $value;
		}
	}

	/**
	 * 404 action
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		return Response::forge(ViewModel::forge('404'), 404);
	}
}
