<?php
use \Model_Rider;

class Controller_Rest_Riders extends Controller_Rest
{
	protected $format = "json";

	/*
	 * データ取得
	 */
	public function get_index()
	{
		if(Input::get('id'))
		{
			$id = Input::get('id');
			$rider = Model_Rider::find($id);
			
			$this->response($rider);
		}else if(Input::get('oa_type')){
			$queryOption = array();
			foreach(Input::get() as $k => $v)
			{
				$queryOption["where"][] = array($k, $v);
			}
			
			$riders = Model_Rider::find("all",$queryOption);
			
			$this->response($riders);
		}else{
			$riders = Model_Rider::find('all');
			
			$this->response($riders);
		}
	}
 
	/*
	 * データ保存
	 */
	public function post_index()
	{
//		$val = Model_Rider::validate('create');
//		
//		if ($val->run(Input::json()))
//		{
			if(Input::json('data.id'))
			{
				// UPDATE
				$rider = Model_Rider::find(Input::json('data.id'));
				
				$rider->rider_name = Input::json('data.rider_name');
				$rider->oa_type = Input::json('data.oa_type');
				
			}else{
				// INSERT
				$rider = Model_Rider::forge(array(
					'rider_name' => Input::json('data.rider_name'),
					'oa_type' => Input::json('data.oa_type'),
				));

			}
			
			if ($rider and $rider->save())
			{
				$this->response($rider);
			}else{
				$this->response(array(
					'error' => true,
				));
			}
 
//		}else{
//			$this->response(array(
//				'error' => true,
//			));
//		}
			
	}
}