#!/bin/sh

chmod 777 ./fuel/app/config
chmod -R 777 ./fuel/app/cache
chmod -R 777 ./fuel/app/logs
chmod -R 777 ./fuel/app/tmp
