/*
 * 一覧View
 */

/* 昭和のライダー一覧 */
var Dom1View = AppView.extend({
	el: "#Dom_1",
	
	events: {
		"click .Get" : "update"
	},

	initialize: function () {
		Dom1View.__super__.initialize.apply(this, arguments);
		
		this.render();
	},

	render: function () {
		this.model.fetch({
			data:{"oa_type":"昭和"},
			dataType : 'json',
			success : $.proxy(this.add, this)
		});
	},
	
	add : function(collection, resp) {
		var that = this;
		
		_.each(resp, function(row){
			$(that.el).append(_.template($('#rider_row_template').html(), row));
		})
	},
	
	update : function(){
		$(this.el).find("tr[data-row^='rider']").remove();
		this.render();
	}
	
});

/* 平成のライダー一覧 */
var Dom2View = AppView.extend({
	el: "#Dom_2",

	events: {
		"click .Get" : "update"
	},

	initialize: function () {
		Dom2View.__super__.initialize.apply(this, arguments);
		
		this.render();
	},

	render: function () {
		this.model.fetch({
			data:{"oa_type":"平成"},
			dataType : 'json',
			success : $.proxy(this.add, this)
		});
	},
	
	add : function(collection, resp) {
		var that = this;
		
		_.each(resp, function(row){
			$(that.el).append(_.template($('#rider_row_template').html(), row));
		})
	},
	
	update : function(){
		$(this.el).find("tr[data-row^='rider']").remove();
		this.render();
	}
	
});
