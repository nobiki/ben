/*
 * 追加フォームView
 */

var Sample2View = AppView.extend({
	el: "#add_form",

	events: {
		"click button" : "add"
	},

	initialize: function () {
		Sample2View.__super__.initialize.apply(this, arguments);
		
		this.render();
	},

	render: function () {
	},
	
	add: function(e){
		var Rider = this.model;	// Modelを別変数に入れておく
		var postData = {};			// POSTするデータ変数（初期化）
		
		// フォームから値を拾う
		$($(e.currentTarget).parents("form").serializeArray()).each(function(i, v) {
			postData[v.name] = v.value;
		});

		//console.log("Before save: " + JSON.stringify(Rider));
		
		Rider.save({"data":postData},{
			success : function(){
				alert("保存に成功");
			},
			error : function(){
				alert("保存に失敗");
			}
		});
		
		//console.log("After save: " + JSON.stringify(Rider));
		
		return false;
	}
});