/*
 * Nuvi.js - Version: 0.8a
 *
 * Sample:
 *	<script src="nuvi.js"></script>
 *	<script>
 *	$(function(){
 *	$(window).load(function(){
 *		var nuvi = new Nuvi({
 *			triger: $('#slide'),
 *			slider: $('#slider'),
 *			body: $('#slider-body'),
 *			container: 'div.slider-container',	// $slider.find(args.container)
 *
 *			styles: {
 *				slider: {
 *					"position": "absolute",
 *					"margin": "0",
 *					"padding": "0",
 *					"top": "0",
 *					"left": "-240px",
 *					"width": "240px",
 *					"height": "100%",
 *					"background-color": "#000000"
 *				},
 *				
 *				body: {
 *					"position": "relative",
 *					"float": "left",
 *					"width": "240px",
 *					"background-color": "#888888"
 *				},
 *				
 *				container: {
 *					"position": "absolute",
 *					"top": "0",
 *					"left": "0",
 *					"width": "240px",
 *					"height": "0px",
 *					"-webkit-transform":"translate3d(0,0,0)"
 *				}
 *			}
 *		});
 *	});
 *	</script>
 */

var Nuvi = function(args){
	// 引数取得
	$triger     = args.triger,
	$slider     = args.slider,
	$body       = args.body,
	$container  = $slider.find(args.container);
	
	$('body').prepend("<div id=\"overlay\"></div>");		//フタ

	this.setStyle(args.styles);
	this.setTrigger(args.styles.slider.width);
	
	this.init();
}

Nuvi.prototype = {
	
	setStyle:function(styles){
		$("body").css("position", "static");
		
		// $slider
		if(styles.slider)
		{
			for(var i in styles.slider){
				$slider.css(i, styles.slider[i])
			}		
		}
		
		// $body
		if(styles.body)
		{
			for(var i in styles.body){
				$body.css(i, styles.body[i])
			}		
		}
		
		// $container
		if(styles.container)
		{
			for(var i in styles.container){
				$container.css(i, styles.container[i])
			}		
		}		
	},
	
	setTrigger:function(width){
		/*
		 * メニューの開閉
		 */
		$($triger).on('click',function(){
			var tmp = $($slider).position();
			if(parseInt(tmp.left) < 0)
			{
				// 開く
				$($slider).css({
					"-webkit-transform":"translate3d("+width+",0,0)",
					"-webkit-transition":"-webkit-transform 300ms cubic-bezier(0,0,0.25,1)"
				});
				$("body").css("margin-left", width);
				
				// コンテンツ側にはフタをする
				$('#overlay').css("position", "fixed");
				$('#overlay').css("top", "0");
				$('#overlay').css("left", "0");
				$('#overlay').css("width", "100%");
				$('#overlay').css("height", "100%");
				$('#overlay').css("background", "#000");
				$('#overlay').css("opacity", "0.1");
				$('#overlay').css("filter", "alpha(opacity=10)");
				$('#overlay').show();
				
			}else{
				// 閉じる
				$($slider).css({
					"-webkit-transform":"translate3d(0,0,0)",
					"-webkit-transition":"-webkit-transform 300ms cubic-bezier(0,0,0.25,1)"
				});
				$("body").css("margin-left", "0px");
				$('#overlay').hide();
			}
		});
		
	},
	
	init:function(){

		//移動距離を指定するのに使う
		var distance = 0;    

		//スライド関数
		var slide = {

			//スライド(進む)
			next: function (index, spd, flick_flg) {
				distance = distance + index;
				slide.scroll(distance, spd, flick_flg);
			},

			//スライド(戻る)
			prev: function (index, spd, flick_flg) {
				distance = distance - index;
				slide.scroll(distance, spd, flick_flg);
			},

			//移動距離分スクロール
			scroll : function (d, spd, flick_flg) {
				var move = -d

				var env = 'translate3d(0,' + move + 'px,0)';

				if (flick_flg) {
					/* フリック時はwebkit-transformプロパティを設定し、滑らかなアニメーションにする */
					transit_property = '-webkit-transform ' + spd + 'ms cubic-bezier(0,0,0.25,1)';
				} else {
					transit_property = 'none';
				}

				$container.css({
					'-webkit-transform':env,
					'-webkit-transition':transit_property
				}).bind('webkitTransitionEnd', function(){
					//ここで移動後の終了イベントが取れます     
				});
			}
		}

		//初期化
		var pageX;      //リアルタイムのX座標
		var pageY;      //リアルタイムのY座標
		var startPageX; //フリックスタート時のX 座標の位置
		var startPageY; //フリックスタート時のY 座標の位置
		var endPageX;   //フリックエンド時のX 座標の位置
		var endPageY;   //フリックエンド時のY 座標の位置
		var rangePageX; //X 座標の移動距離
		var rangePageY; //Y 座標の移動距離
		var startTime;  //スタート時の時間
		var move_time = 0;
		var menuHeight = $($body).height();  //メニュー部分の高さ
		
		/*
		 * メニュー部分のフリック＆スライド処理
		 */
		$($slider).on({
			'touchstart': function()
			{
				//event.preventDefault( ) ;		//memo:切ると<a>タグが反応しなくなる
				startPageX = event.changedTouches[0].pageX;
				startPageY = event.changedTouches[0].pageY;
				pageX = startPageX;
				pageY = startPageY;
				startTime = +new Date();
				//console.log("[start] x:"+startPageX+",y:"+startPageY);
			}
		});

		$($slider).on({
			'touchmove': function()
			{
				event.preventDefault( ) ;
				var moveY = event.changedTouches[0].pageY;
				var absY = Math.abs(pageY - moveY);         // 移動距離の絶対値
				var spd = 0.5;
				pageX = event.changedTouches[0].pageX;

				/* スワイプ処理 */
				if (pageY > moveY) {
				  slide.next(absY, spd, false);
				} else if (pageY < moveY) {
				  slide.prev(absY, spd, false);
				}
				pageY = moveY;
			}
		});

		$($slider).on({
			'touchend': function()
			{
				//event.preventDefault( ) ;		//memo:切ると<a>タグが反応しなくなる
				endPageX = event.changedTouches[0].pageX;
				endPageY = event.changedTouches[0].pageY;
				pageX = endPageX;
				pageY = endPageY;

				var diffY = startPageY - endPageY;

				//フリック移動距離
				rangePageX = parseInt(endPageX) - parseInt(startPageX);
				rangePageY = parseInt(endPageY) - parseInt(startPageY);
				var mv = Math.abs(rangePageY) /* 絶対値 */
				//console.log("[range] x:"+rangePageX+",y:"+rangePageY);

				//フリックスピード（目算） mv = フリック距離(px)、 spd = 移動スピード(ms)
				if(mv < 100)  
				{
					var spd = 400;
				}else if(mv < 400){
					var spd = 1000;
				}else{
					var spd = 1800;
				}

				var now = +new Date();          //現在時間
				var diffTime = now - startTime; //touchstartからの経過時間
				var maxTop = 0;   /* 上限 */

				/* 下限 */
				// TODO:menuHeight < getSide() の時に、maxBottomがマイナスになる？
				if("yoko" == getOrientation())
				{
					var maxBottom = menuHeight - getShortSide();
				}else if("tate" == getOrientation()){
					var maxBottom = menuHeight - getLongSide();
				}


				/* フリック処理(touchstartからの経過時間が短い場合) */
				if (diffTime < 200) {
					if (diffY > 0) {
						//下限を超えた時はToを補正
						if((distance + mv) > maxBottom)
						{
							//distance = distance - maxBottom;
							mv = (distance - maxBottom) * -1;
							spd = 200;
						}
						slide.next(mv, spd, true);
					} else if (diffY < 0) {
						//上限を超えた時はToを補正
						if((distance - mv) < maxTop)
						{
							distance = maxTop;
							mv = maxTop;
							spd = 200;
						}
						slide.prev(mv, spd, true);
					}

				/* スクロール処理(touchstartからの経過時間が長い場合) */
				}else{
					//上限を超えた時はToを補正
					if(distance < maxTop)
					{
						distance = maxTop;
						slide.prev(maxTop, 400);

					//下限を超えた時はToを補正
					}else if(distance > maxBottom){
						var tmp = (distance - maxBottom) * -1;
						slide.next(tmp, 400);
					}else{
					//どちらでもない時は何もしない
					}
				}
				move_time = 0;
			}
		});

		/*
		 * 画面が回転した時の処理
		 */
		$(window).on('orientationchange', function(){

			/* 下限 */
			// TODO:menuHeight < getSide() の時に、maxBottomがマイナスになる？
			if("yoko" == getOrientation())
			{
				var maxBottom = menuHeight - getShortSide();
			}else if("tate" == getOrientation()){
				var maxBottom = menuHeight - getLongSide();
			}

			//alert(getOrientation()+"|"+maxBottom+"|"+distance);

			// 下限調整 (memo:上限はどちらも0なので調整する必要なし)
			if(distance > maxBottom){
				var tmp = (distance - maxBottom) * -1;
				slide.next(tmp, 400);
			}
		});

		/*
		 * ずらした部分をタップした時にメニューを閉じる処理
		 */
		$("#overlay").on('click', function(){
			// 閉じる
			$($slider).css({
				"-webkit-transform":"translate3d(0,0,0)",
				"-webkit-transition":"-webkit-transform 300ms cubic-bezier(0,0,0.25,1)"
			});
			$("body").css("margin-left", "0px");
			$('#overlay').hide();
		});

		/*
		 * ずらした部分は固定する
		 */
		$("#overlay").on({
			'touchmove': function()
			{
				event.preventDefault( ) ;
			}
		});
	}
	
}



/*
 * 機種判別
 */
function getDevice(){
  if(navigator.userAgent.indexOf('iPhone') > 0 || navigator.userAgent.indexOf('iPad') > 0 || navigator.userAgent.indexOf('iPod') > 0){
    return 'iOS';
  }else if(navigator.userAgent.indexOf('Android') > 0){
    return 'Android';
  }else{
    return 'Other';
  }
}

/*
 * 縦横判別
 */
function getOrientation(){
  if(Math.abs(window.orientation) == 90)
  {
    return "yoko";
  }else{
    return "tate";
  }
}

/*
 * 長辺取得
 */
function getLongSide(){
  var ret = [];
  ret['longSide']  = 0;

  if("iOS" == getDevice())
  {
    if("yoko" == getOrientation())
    {
      ret['longSide']  = window.innerWidth;
    }else if("tate" == getOrientation()){
      ret['longSide']  = window.innerHeight;
    }
  }else if("Android" == getDevice()){
    if("yoko" == getOrientation())
    {
      ret['longSide']  = window.innerWidth;
    }else if("tate" == getOrientation()){
      ret['longSide']  = window.innerHeight;
    }
  }
  
  return ret['longSide'];
}

/*
 * 短辺取得
 */
function getShortSide(){
  var ret = [];
  ret['shortSide'] = 0;

  if("iOS" == getDevice())
  {
    if("yoko" == getOrientation())
    {
      ret['shortSide'] = window.innerHeight;
    }else if("tate" == getOrientation()){
      ret['shortSide'] = window.innerWidth;
    }
  }else if("Android" == getDevice()){
    if("yoko" == getOrientation())
    {
      ret['shortSide'] = window.innerHeight;
    }else if("tate" == getOrientation()){
      ret['shortSide'] = window.innerWidth;
    }
  }

  return ret['shortSide'];
}

/*
 * 長辺・短辺取得
 */
function getLongShort(){
  var ret = [];
  ret['shortSide'] = getShortSide();
  ret['longSide']  = getLongSide();

  return ret;
}