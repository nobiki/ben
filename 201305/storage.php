<?php
	$aaa = "?".date("YmdHis", strtotime('now'));
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<!-- Google Libraries API start -->
<script src="http://www.google.com/jsapi"></script>
<script>google.load("jquery", "1.6");</script>
<!-- Google Libraries API end -->

<script type="text/javascript">
$(document).ready(function(){
  load(1);
  load(2);
  load(3);
});

function load(key) {
  var gazo = window.localStorage.getItem(key);
  if(!gazo)
  {
	$("#pics"+key).html("リモートアクセス：<br /><img src='./"+key+".jpg<?php print $aaa; ?> alt='remote' />");
  }else{
	$("#pics"+key).html("ローカルアクセス：<br /><img src='"+gazo+"' alt='local' />");
  }
}

function encodeToDataURL(key, url) {
	var response_data = "";

	jQuery.ajax({
		type: "GET",
		url: "convertdataurl.php?url="+url,
		dataType: "text",
		timeout: 20000,
		success: function (data, status, xhr) {
			response_data = data;
			window.localStorage.setItem(key, response_data);
			alert("LocalStorageに"+key+"つ目の画像を保存しました");
		}
	});
}

function del(key){
	localStorage.removeItem(key)
	alert("LocalStorageから"+key+"つ目の画像を削除しました");
}


</script>

</head>
<body>

<div id="pics1"></div>
<br clear="all" />
<input type="button" value="この画像をLocalStorageに保存" onClick="encodeToDataURL(1, './1.jpg');">
<input type="button" value="この画像をLocalStorageから削除" onClick="del(1);">
<br clear="all" />

<br />
<br />
 
<div id="pics2"></div>
<br clear="all" />
<input type="button" value="この画像をLocalStorageに保存" onClick="encodeToDataURL(2, './2.jpg');">
<input type="button" value="この画像をLocalStorageから削除" onClick="del(2);">
<br clear="all" />

<br />
<br />
 
<div id="pics3"></div>
<br clear="all" />
<input type="button" value="この画像をLocalStorageに保存" onClick="encodeToDataURL(3, './3.jpg');">
<input type="button" value="この画像をLocalStorageから削除" onClick="del(3);">
<br clear="all" />

<br />
<br />
 
 

</body>
</html>
